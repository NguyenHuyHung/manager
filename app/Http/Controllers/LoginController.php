<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DB;
use Session;
use Auth;
use DateTime;
class LoginController extends Controller
{
	
	public function LoginGetShow() {
		
		return view('auth.login');
	}
	
	public function RegisterGetShow() {
	
		return view('auth.register');
	}
	public function PostRegister(Request $request) {
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		$user->remember_token = str_random(100);
		$user->created_at = new DateTime();
		$user->updated_at = new DateTime();
		$user->save();
		logger($user->name);
		logger($user->email);
		logger($user->password);
		logger($user->remember_token);
		logger($user->created_at);
		logger($user->updated_at);
		
		return response()->json(array('status'=> "Register thanh công"), 200);
	}
	
	public function PostLogin(Request $request) {
// // // 		$users = User::all();
// // 		//$users = DB::select('select * from users');
// 		$msg = 'success';

		
		$users = User::select('id','email')->orderBy('id','DESC')->get()->toArray();
		$token = Session::token();
		logger($token);
		$email = $request['email'];
		$password = $request['password'];
		logger($users);
		logger($email);
		logger($password);
		if (Auth::attempt(['email' => $email, 'password' => $password])) {
			return response()->json(array('status'=> "hung", 'users' => $users, 'token' => $token), 200);
		}else {
			return response()->json(array('status'=> "Error Check Login"), 200);
		}
		
// 		logger($users);
		
// 		//return response()->json($users, 200);
// 		return response()->json(array('status'=> $msg,'users' => $users), 200);
		

	}
	
	
	public function loginTest() {
		return response()->json(array('status'=> "hung"), 200);

	}
	
	public function GetLogout() {
		
		Auth::logout();
		return response()->json(array('status'=> "logout",'token'=>$token), 200);
	
	}
}

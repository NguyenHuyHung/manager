<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    			'name' => 'hung',
    			'email' => 'zayphong92@gmail.com',
    			'password' => bcrypt('11111111'),
    			'created_at' =>new DateTime(),
    			'updated_at' =>new DateTime(),
    	]);
    }
}
